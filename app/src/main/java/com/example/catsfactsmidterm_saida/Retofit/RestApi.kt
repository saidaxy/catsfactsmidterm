package com.example.catsfactsmidterm_saida.Retofit

import com.example.catsfactsmidterm_saida.Model.CatModel
import retrofit2.Call
import retrofit2.http.GET
interface RestApi {

    @GET("/facts")
    fun getAllCats() : Call<List<CatModel>>
}