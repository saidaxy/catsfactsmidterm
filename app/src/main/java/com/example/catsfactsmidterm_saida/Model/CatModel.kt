package com.example.catsfactsmidterm_saida.Model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Cats")
data class CatModel (

    @PrimaryKey
    var fact : String,
    var upvotes : Int

)