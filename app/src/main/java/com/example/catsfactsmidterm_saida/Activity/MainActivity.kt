
package com.example.catsfactsmidterm_saida.Activity

import CatRecyclerViewAdapter
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.catsfactsmidterm_saida.Data.CatViewModel
import com.example.catsfactsmidterm_saida.Model.CatModel
import com.example.catsfactsmidterm_saida.R
import com.example.catsfactsmidterm_saida.R.id
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    lateinit var catRecyclerView: RecyclerView
    lateinit var CatViewModel: CatViewModel ///by  viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        catRecyclerView = findViewById(id.catRecyclerView)

        CatViewModel = ViewModelProviders.of(this).get(CatViewModel::class.java)


        if(isNetworkConnected(this))
        {
            CatViewModel.getCatsFromAPIAndStore()

        }
        else
        {
            Toast.makeText(this,"No internet found. Showing cached list in the view", Toast.LENGTH_LONG).show()
        }

        CatViewModel.getAllCatList().observe(this, Observer<List<CatModel>> { catList ->
            Log.e(MainActivity::class.java.simpleName,catList.toString())
            setUpCatRecyclerView(catList!!)
        })


    }


    fun isNetworkConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }


    fun setUpCatRecyclerView(cats : List<CatModel>)
    {
        val countryRecyclerViewAdapter = CatRecyclerViewAdapter(this, cats)
        catRecyclerView.adapter = countryRecyclerViewAdapter
        catRecyclerView.layoutManager = GridLayoutManager(this,2)
        catRecyclerView.setHasFixedSize(true)
    }
}


