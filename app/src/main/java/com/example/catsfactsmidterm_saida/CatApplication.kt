package com.example.catsfactsmidterm_saida

import android.app.Application
import androidx.room.Room
import com.example.catsfactsmidterm_saida.DB.CatDatabase

class CatApplication : Application() {

    companion object {
        var database: CatDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        database =  Room.databaseBuilder(applicationContext, CatDatabase::class.java, "cat_db").fallbackToDestructiveMigration().build()
    }
}