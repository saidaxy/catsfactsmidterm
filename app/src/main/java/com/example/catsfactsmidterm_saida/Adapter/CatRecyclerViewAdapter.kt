import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.catsfactsmidterm_saida.Model.CatModel
import com.example.catsfactsmidterm_saida.R

class CatRecyclerViewAdapter(_context : Context, _catList:List<CatModel>) : RecyclerView.Adapter<CatRecyclerViewAdapter.CatViewHolder>() {


    override fun onBindViewHolder(holder: CatRecyclerViewAdapter.CatViewHolder, position: Int) {
        val cat = catList[position]
        holder.catFact.text = cat.fact
        holder.catUpvotes.text = cat.upvotes.toString()
    }

    val context = _context
    val catList = _catList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.single_cat_model_layout, parent, false)
        return CatViewHolder(view)
    }

    override fun getItemCount(): Int {
        return catList.size
    }

    class CatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val catFact: TextView = itemView.findViewById(R.id.cat_fact)
        val catUpvotes: TextView = itemView.findViewById(R.id.cat_upvotes)
        val catImage: ImageView = itemView.findViewById(R.id.cat_image)
    }

}





