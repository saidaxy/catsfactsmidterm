package com.example.catsfactsmidterm_saida.di

import androidx.room.Room
import com.example.catsfactsmidterm_saida.DB.CatDao
import com.example.catsfactsmidterm_saida.DB.CatDatabase
import com.example.catsfactsmidterm_saida.Data.CatRepository
import com.example.catsfactsmidterm_saida.Data.CatViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/*
val dbModule = module {
    single { Room.databaseBuilder(androidContext(),
        CatDatabase::class.java, CatDatabase.DB_NAME).build() }

    single { get<CatDatabase>().catDao() }
}

val repoModule = module {
    single { CatRepository(get() as CatDao) }
}

val viewModelModule = module {
    viewModel { CatViewModel(get() as CatRepository) }
    viewModel { CatViewModel(get() as CatRepository) }
}

 */