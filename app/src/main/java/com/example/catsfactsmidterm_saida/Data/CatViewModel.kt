package com.example.catsfactsmidterm_saida.Data

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.catsfactsmidterm_saida.Model.CatModel

class CatViewModel : ViewModel() {

    lateinit var CatRepository: CatRepository

    init {
        CatRepository = CatRepository()
    }

    fun getAllCatList(): LiveData<List<CatModel>> {
        return CatRepository.getCats()
    }

    fun getCatsFromAPIAndStore() {
        CatRepository.ApiCallAndPutInDB()
    }
}


