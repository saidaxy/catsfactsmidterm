package com.example.catsfactsmidterm_saida.Data

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.catsfactsmidterm_saida.CatApplication
import com.example.catsfactsmidterm_saida.DB.CatDao
import com.example.catsfactsmidterm_saida.Model.CatModel
import com.example.catsfactsmidterm_saida.Retofit.RestApi
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CatRepository(): ICatRepository {
    val BASE_URL = "https://cat-fact.herokuapp.com"
    val TAG = CatRepository::class.java.simpleName

    override fun getCats(): LiveData<List<CatModel>> {
        return CatApplication.database!!.catDao().getAllCats()

    }

    override fun ApiCallAndPutInDB() {
        val gson = Gson()
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL)
            .build()

        val restApi = retrofit.create<RestApi>(RestApi::class.java)

            restApi.getAllCats().enqueue(object : Callback<List<CatModel>> {

                override fun onFailure(call: Call<List<CatModel>>?, t: Throwable?) {
                    Log.e(TAG, "OOPS!! something went wrong..")
                }

                override fun onResponse(
                    call: Call<List<CatModel>>?,
                    response: Response<List<CatModel>>?
                ) {

                    Log.e(TAG, response!!.body().toString())
                    when (response.code()) {
                        200 -> {
                            Thread(Runnable {

                                CatApplication.database!!.catDao()
                                    .deleteAllCats()
                                CatApplication.database!!.catDao()
                                    .insertAllCats(response.body()!!)

                            }).start()
                        }
                    }

                }
            })

        }

}
