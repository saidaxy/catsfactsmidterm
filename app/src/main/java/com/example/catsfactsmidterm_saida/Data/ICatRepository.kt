package com.example.catsfactsmidterm_saida.Data

import androidx.lifecycle.LiveData
import com.example.catsfactsmidterm_saida.Model.CatModel

interface ICatRepository {

    fun getCats() : LiveData<List<CatModel>>
    fun ApiCallAndPutInDB()
}