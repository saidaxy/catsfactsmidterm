package com.example.catsfactsmidterm_saida.DB

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.catsfactsmidterm_saida.Model.CatModel

@Dao
interface CatDao {

    @Query("SELECT * FROM Cats")
    fun getAllCats() : LiveData<List<CatModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllCats(catList: List<CatModel>)

    @Query("DELETE FROM Cats")
    fun deleteAllCats()
}