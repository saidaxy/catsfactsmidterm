package com.example.catsfactsmidterm_saida.DB

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.catsfactsmidterm_saida.Model.CatModel


@Database(entities = [(CatModel::class)], version = 1)
abstract class CatDatabase : RoomDatabase(){

    abstract fun catDao() : CatDao
}